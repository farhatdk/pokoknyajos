import {StyleSheet} from 'react-native';

module.exports = StyleSheet.create({
  wrapper: {
    paddingTop: 40,
    paddingBottom: 50,
    paddingRight: 5,
    paddingLeft: 5,
  },
  top: {
    alignItems: 'center',
    flex: 1,
  },
  content: {
    alignSelf: 'center',
    alignItems: 'center',
    flex: 2,
  },
  contentSpaceBetween: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flex: 2,
  },
  title: {
    color: '#810000',
    fontSize: 58,
    fontWeight: '700',
    fontFamily: 'Roboto',
  },
  numDigits: {
    color: '#d7933f',
    fontSize: 24,
    textAlign: 'center',
  },
});
