const questionsSrc = [
  {
    question: 'Dibawah ini suku yang terdapat di pulau Sumatera, kecuali ?',
    choices: ['Minangkabau', 'Melayu', 'Betawi', 'Aceh'],
    answer: 'Betawi',
  },
  {
    question: 'Apa nama rumah adat dari suku Jawa ?',
    choices: ['Rumah Limas', 'Rumah Joglo', 'Rumah Honai', 'Rumah Kasepuhan'],
    answer: 'Rumah Joglo',
  },
  {
    question: 'Dimanakah letak suku Minahasa ?',
    choices: ['Sumatera Barat', 'Kalimantan Utara', 'Sulawesi Utara', 'Madura'],
    answer: 'Sulawesi Utara',
  },
  {
    question: 'Berapakah jumlah total suku Minangkabau ?',
    choices: ['1.237.177', '3.946.416', '1.041.925', '6.462.713'],
    answer: '6.462.713',
  },
  {
    question: 'Dibawah ini jenis dari suku Batak, kecuali ?',
    choices: ['Karo', 'Toba', 'Angkola', 'Anak Dalam'],
    answer: 'Anak Dalam',
  },
  {
    question: 'Bahasa daerah apakah yang digunakan pada daerah Cirebon ?',
    choices: [
      'Bahasa Cirebon',
      'Bahasa Melayu',
      'Bahasa Jawa',
      'Bahasa Betawi',
    ],
    answer: 'Bahasa Cirebon',
  },
  {
    question: 'Mata pencaharian utama dari suku Makassar adalah ?',
    choices: ['Nelayan', 'Bertani', 'Berladang', 'Berburu'],
    answer: 'Nelayan',
  },
  {
    question: 'Dibawah ini yang manakah jenis dari suku Papua ?',
    choices: ['Semua Benar', 'Abau', 'Abra', 'Adora'],
    answer: 'Semua Benar',
  },
  {
    question: 'Suku yang termasuk ke dalam kategori suku yang langka adalah ?',
    choices: ['Samin', 'Betawi', 'Melayu', 'Banjar'],
    answer: 'Samin',
  },
  {
    question: 'Apa nama rumah adat dari suku Banjar ? ',
    choices: ['Rumah Gadang', 'Rumah Baanjung', 'Rumoh Aceh', 'Saoraja'],
    answer: 'Rumah Baanjung',
  },
  {
    question: 'Suku apakah yang jumlah totalnya sekitar 36.701.670 jiwa ?',
    choices: ['Sunda', 'Madura', 'Jawa', 'Betawi'],
    answer: 'Sunda',
  },
  {
    question:
      'Dibawah ini adalah yang bukan mata pencaharian suku betawi, kecuali',
    choices: ['Semua Benar', 'Pegawai', 'Pedagang', 'Buruh'],
    answer: 'Semua Benar',
  },
  {
    question: 'Dari jenis suku apakah Osing dan Tengger ?',
    choices: ['Batak', 'Jawa', 'Cirebon', 'Sunda'],
    answer: 'Jawa',
  },
  {
    question: 'Disebut apakah nama rumah adat dari suku Aceh ?',
    choices: [
      'Rumoh Aceh',
      'Balai Batak Toba',
      'Rumah Limas',
      'Rumah Adat Kebaya',
    ],
    answer: 'Rumoh Aceh',
  },
  {
    question: 'Bahasa daerah apa yang digunakan oleh suku Banten ?',
    choices: ['Bahasa Sunda', 'Bahasa Batak', 'Bahasa Betawi', 'Bahasa Gayo'],
    answer: 'Bahasa Sunda',
  },
  {
    question: 'Dibawah ini yang termasuk suku langka, kecuali ?',
    choices: ['Banjar', 'Kombai', 'Samin', 'Mante '],
    answer: 'Banjar',
  },
  {
    question: 'Dibawah ini yang bukan dari bahasa suku Aceh adalah ?',
    choices: [
      'Bahasa Betawi',
      'Bahasa Alas',
      'Bahasa Aneuk Jamee',
      'Bahasa Gayo',
    ],
    answer: 'Bahasa Betawi',
  },
  {
    question: 'Berapakah jumlah suku Aceh ? ',
    choices: ['1.381.660', '1.415.547', '4.091.451', '8.466.969'],
    answer: '4.091.451',
  },
  {
    question: 'Apakah nama rumah adat dari suku Palembang ? ',
    choices: ['Rumah Aceh', 'Rumah Limas', 'Rumah Adat Kebaya', 'Omo Hada'],
    answer: 'Rumah Limas',
  },
  {
    question: 'Dipulau manakah letak suku Banten ? ',
    choices: ['Pulau Papua', 'Pulau Kalimantan', 'Pulau Jawa', 'Pulau Bali'],
    answer: 'Pulau Jawa',
  },
  {
    question: 'Ada berapa total suku langka yang ada di Indonesia ?',
    choices: ['8', '9', '7', '4'],
    answer: '9',
  },
  {
    question: 'Dimana letak suku Nias ? ',
    choices: [
      'Sumatera Barat',
      'Pulau Nias',
      'Sumatera Selatan',
      'Sulawesi Selatan',
    ],
    answer: 'Pulau Nias',
  },
  {
    question: 'Apa nama rumah adat dari suku Betawi ?',
    choices: [
      'Rumah Adat kebaya',
      'Rumah Kasepuhan',
      'Balai Batak Toba',
      'Rumah Panggung',
    ],
    answer: 'Rumah Adat kebaya',
  },
  {
    question: 'Ada berapakah jenis suku yang ada di suku Batak ?',
    choices: ['9', '8', '2', '5'],
    answer: '8',
  },
  {
    question: 'Apa nama bahasa daerah suku Banjar ?',
    choices: [
      'Bahasa Melayu',
      'Bahasa Banjar',
      'Bahasa Minang',
      'Bahasa Lampung',
    ],
    answer: 'Bahasa Banjar',
  },
  {
    question:
      'Mata pencaharian apa yang paling banyak dilakukan oleh suku-suku di Indonesia ?',
    choices: ['Bertani dan Berladang', 'Nelayan', 'Berburu', 'Pengrajin'],
    answer: 'Bertani dan Berladang',
  },
  {
    question: 'Dibawah ini manakah yang termasuk jenis dari suku Aceh ?',
    choices: ['Gayo Luwes', 'Gayo Lut', 'Kluet', 'Semua Benar'],
    answer: 'Semua Benar',
  },
  {
    question: 'Bahasa apakah yang digunakan oleh suku Madura ? ',
    choices: ['Bahasa Madura', 'Bahasa Aceh', 'Bahasa Sunda', 'Bahasa Cirebon'],
    answer: 'Bahasa Madura',
  },
  {
    question: 'Berada di pulau manakah suku Banjar dan Dayak berada ?',
    choices: ['Sulawesi', 'Papua', 'Kalimantan', 'Bali'],
    answer: 'Kalimantan',
  },
  {
    question:
      'Suku apakah yang jumlahnya menjadi suku terbanyak di Indonesia ? ',
    choices: ['Bali', 'Banjar', 'Madura', 'Jawa'],
    answer: 'Jawa',
  },
  {
    question: 'Dimanakah letak suku Minangkabau berada ? ',
    choices: ['Sulawesi Utara', 'Sumatera Selatan', 'Sumatera Barat', 'Bali'],
    answer: 'Sumatera Barat',
  },
];

export default questionsSrc;
