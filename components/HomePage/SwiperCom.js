import * as React from 'react';
import Sound from 'react-native-sound';
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  Image,
  Modal,
  TouchableOpacity,
} from 'react-native';
import Swiper from 'react-native-swiper';
import styles from '../styling2';
import TouchableScale from 'react-native-touchable-scale';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const buttonPress = new Sound(require('../../assets/sounds/click_sound.mp3'));
const back = require('../../assets/images/backwhite.webp');
const Dayak = require('../../assets/images/Dayak.jpg');
const Dayak2 = require('../../assets/images/Dayak2.jpg');
const Dayak3 = require('../../assets/images/Dayak3.jpg');
const Minahasa = require('../../assets/images/Minahasa.webp');
const Minahasa2 = require('../../assets/images/Minahasa2.webp');
const Minahasa3 = require('../../assets/images/Minahasa3.webp');
const Batak = require('../../assets/images/Batak.jpg');
const Batak2 = require('../../assets/images/Batak2.jpg');
const Batak3 = require('../../assets/images/Batak3.jpg');
const Bali = require('../../assets/images/Bali.jpg');
const Bali2 = require('../../assets/images/Bali2.jpg');
const Bali3 = require('../../assets/images/Bali3.jpg');

class SwiperCom extends React.Component {
  state = {
    isModalVisible: false,
    isModalVisible2: false,
    isModalVisible3: false,
    isModalVisible4: false,
  };
  openModal() {
    this.setState({isModalVisible: true});
  }
  openModal2() {
    this.setState({isModalVisible2: true});
  }
  openModal3() {
    this.setState({isModalVisible3: true});
  }
  openModal4() {
    this.setState({isModalVisible4: true});
  }
  sound() {
    buttonPress.play();
  }
  componentDidMount() {
    this.setState({isModalVisible: false});
    this.setState({isModalVisible2: false});
    this.setState({isModalVisible3: false});
    this.setState({isModalVisible4: false});
  }

  render() {
    const shadowOpt = {
      width: 100,
      height: 100,
      color: '#000',
      border: 2,
      radius: 3,
      opacity: 0.2,
      x: 0,
      y: 3,
      style: {marginVertical: 5},
    };

    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <TouchableScale
          activeScale={1.05}
          friction={5}
          onPress={() => {
            this.sound(), this.openModal();
          }}>
          <View style={styles.suku3}>
            <ImageBackground
              imageStyle={{borderRadius: 15}}
              style={styles.boximg}
              source={Dayak}>
              <View style={styles.layer} />
              <Text style={styles.textgaleri}>Dayak</Text>
            </ImageBackground>
            <Modal
              animationType="fade"
              visible={this.state.isModalVisible}
              transparent={true}
              onRequestClose={() => this.componentDidMount()}>
              <View style={styles.modal1}>
                <View style={styles.modal3}>
                  <View style={{zIndex: 2, position: 'absolute'}}>
                    <Text style={styles.teksin}>Dayak</Text>
                    <TouchableOpacity onPress={() => this.componentDidMount()}>
                      <Image
                        hitSlop={{top: 10, left: 30}}
                        style={styles.backmod}
                        source={back}
                      />
                    </TouchableOpacity>
                  </View>
                  <Swiper
                    paginationStyle={styles.pagination}
                    showsPagination={true}
                    showsButtons={false}
                    loop={false}
                    autoplay={true}
                    autoplayTimeout={3.5}
                    activeDotColor={'white'}
                    activeDotStyle={{width: wp(5)}}
                    dotColor={'white'}
                    in>
                    <Image style={styles.modalbordersm} source={Dayak} />
                    <Image style={styles.modalbordersm} source={Dayak2} />
                    <Image style={styles.modalbordersm} source={Dayak3} />
                  </Swiper>
                </View>
              </View>
            </Modal>
          </View>
        </TouchableScale>
        <TouchableScale
          activeScale={1.05}
          friction={5}
          style={{zIndex: 3}}
          onPress={() => {
            this.sound(), this.openModal2();
          }}>
          <View style={styles.suku3}>
            <ImageBackground
              imageStyle={{borderRadius: 15}}
              style={styles.boximg}
              source={Batak}>
              <View style={styles.layer} />
              <Text style={styles.textgaleri}>Batak</Text>
            </ImageBackground>
            <Modal
              animationType="fade"
              visible={this.state.isModalVisible2}
              transparent={true}
              onRequestClose={() => this.componentDidMount()}>
              <View style={styles.modal1}>
                <View style={styles.modal3}>
                  <View style={{zIndex: 2, position: 'absolute'}}>
                    <Text style={styles.teksin}>Batak</Text>
                    <TouchableOpacity onPress={() => this.componentDidMount()}>
                      <Image
                        hitSlop={{top: 10, left: 30}}
                        style={styles.backmod}
                        source={back}
                      />
                    </TouchableOpacity>
                  </View>
                  <Swiper
                    paginationStyle={styles.pagination}
                    showsPagination={true}
                    showsButtons={false}
                    loop={false}
                    autoplay={true}
                    autoplayTimeout={3.5}
                    activeDotColor={'white'}
                    activeDotStyle={{width: wp(5)}}
                    dotColor={'white'}
                    in>
                    <Image style={styles.modalbordersm} source={Batak} />
                    <Image style={styles.modalbordersm} source={Batak2} />
                    <Image style={styles.modalbordersm} source={Batak3} />
                  </Swiper>
                </View>
              </View>
            </Modal>
          </View>
        </TouchableScale>
        <TouchableScale
          activeScale={1.05}
          friction={5}
          onPress={() => {
            this.sound(), this.openModal3();
          }}>
          <View style={styles.suku3}>
            <ImageBackground
              imageStyle={{borderRadius: 15}}
              style={styles.boximg}
              source={Minahasa}>
              <View style={styles.layer} />
              <Text style={styles.textgaleri}>Minahasa</Text>
            </ImageBackground>
            <Modal
              animationType="fade"
              visible={this.state.isModalVisible3}
              transparent={true}
              onRequestClose={() => this.componentDidMount()}>
              <View style={styles.modal1}>
                <View style={styles.modal3}>
                  <View style={{zIndex: 2, position: 'absolute'}}>
                    <Text style={styles.teksin}>Minahasa</Text>
                    <TouchableOpacity onPress={() => this.componentDidMount()}>
                      <Image
                        hitSlop={{top: 10, left: 30}}
                        style={styles.backmod}
                        source={back}
                      />
                    </TouchableOpacity>
                  </View>
                  <Swiper
                    paginationStyle={styles.pagination}
                    showsPagination={true}
                    showsButtons={false}
                    loop={false}
                    autoplay={true}
                    autoplayTimeout={3.5}
                    activeDotColor={'white'}
                    activeDotStyle={{width: wp(5)}}
                    dotColor={'white'}
                    in>
                    <Image style={styles.modalbordersm} source={Minahasa} />
                    <Image style={styles.modalbordersm} source={Minahasa2} />
                    <Image style={styles.modalbordersm} source={Minahasa3} />
                  </Swiper>
                </View>
              </View>
            </Modal>
          </View>
        </TouchableScale>
        <TouchableScale
          activeScale={1.05}
          friction={5}
          onPress={() => {
            this.sound(), this.openModal4();
          }}>
          <View style={styles.suku4}>
            <ImageBackground
              imageStyle={{borderRadius: 15}}
              style={styles.boximg}
              source={Bali2}>
              <View style={styles.layer} />
              <Text style={styles.textgaleri}>Bali</Text>
            </ImageBackground>
            <Modal
              style={{justifyContent: 'center'}}
              animationType="fade"
              transparent={true}
              visible={this.state.isModalVisible4}
              onRequestClose={() => this.componentDidMount()}>
              <View style={styles.modal1}>
                <View style={styles.modal3}>
                  <View style={{zIndex: 2, position: 'absolute'}}>
                    <Text style={styles.teksin}>Bali</Text>
                    <TouchableOpacity onPress={() => this.componentDidMount()}>
                      <Image
                        hitSlop={{top: 10, left: 30}}
                        style={styles.backmod}
                        source={back}
                      />
                    </TouchableOpacity>
                  </View>
                  <Swiper
                    paginationStyle={styles.pagination}
                    showsPagination={true}
                    showsButtons={false}
                    loop={false}
                    autoplay={true}
                    autoplayTimeout={3.5}
                    activeDotColor={'white'}
                    activeDotStyle={{width: wp(5)}}
                    dotColor={'white'}
                    in>
                    <Image style={styles.modalbordersm} source={Bali2} />
                    <Image style={styles.modalbordersm} source={Bali} />
                    <Image style={styles.modalbordersm} source={Bali3} />
                  </Swiper>
                </View>
              </View>
            </Modal>
          </View>
        </TouchableScale>
      </ScrollView>
    );
  }
}
export default SwiperCom;
