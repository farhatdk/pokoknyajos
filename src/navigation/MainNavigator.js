import React, {Component} from 'react';
import {createAppContainer, NavigationScreenComponent} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {IntroScreen, Home, Penyebaran, Quiz, Detail} from '../screen';
import InfoUnik from '../screen/InfoUnik';

const RootStack = createStackNavigator({
  IntroScreen: IntroScreen,
  Home: Home,
  Quiz: Quiz,
  Penyebaran: Penyebaran,
  Detail: Detail,
  InfoUnik: InfoUnik,
});
export default createAppContainer(RootStack);
