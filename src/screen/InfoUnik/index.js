import * as React from 'react';
import Sound from 'react-native-sound';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  BackHandler,
  Modal,
} from 'react-native';
import Global from '../../global';
import TouchableScale from 'react-native-touchable-scale';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import styles from '../../../components/styling2.js';
import {StatusBar} from 'react-native';
const buttonPress = new Sound(
  require('../../../assets/sounds/click_sound.mp3'),
);
const images = require('../../../assets/images/backgray.webp');
const image = require('../../../assets/images/back-green.webp');

let SQLite = require('react-native-sqlite-storage');
class InfoUnik extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      record: [],
      isModalVisible: false,
    };
  }
  componentDidMount() {
    this.setState({isModalVisible: false});
    this.setState({open: false});
    BackHandler.addEventListener('hardwareBackPress', this.backAction);
    let db = SQLite.openDatabase({
      name: 'infounik.db',
      createFromLocation: '~sukuindo.db',
      location: 'Library',
    });
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM infounik', [], (tx, results) => {
        var len = results.rows.length;
        let data = [];
        for (var i = 0; i < len; i++) {
          var row = results.rows.item(i);
          data.push(row);
        }
        this.setState({
          record: data,
        });
      });
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
  }
  openModal() {
    this.setState({isModalVisible: true});
  }
  backAction = () => {
    this.props.navigation.goBack();
    return true;
  };
  storeData = async value => {
    Global.id_infounik = value;
  };
  sound() {
    buttonPress.play();
  }

  static navigationOptions = ({navigation}) => {
    return {
      title: 'InfoUnik',
      header: null,
      screen: InfoUnik,
    };
  };

  render() {
    const {record} = this.state;
    console.log(Global.id_infounik);
    return (
      <ScrollView>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <View style={styles.view}>
          <TouchableOpacity
            onPress={() => {
              this.sound(), this.props.navigation.goBack();
            }}>
            <Image style={styles.image} source={images} />
          </TouchableOpacity>
          <View style={styles.header}>
            <Text style={styles.teks}>Info Unik</Text>
          </View>
          <View style={styles.infomargin}>
            {record.map((item, index) => (
              <TouchableScale
                activeScale={1.05}
                friction={5}
                onPress={() => {
                  this.sound(), this.openModal(), this.storeData(item.id);
                }}>
                <View style={styles.menu}>
                  <View style={styles.container}>
                    <Text style={styles.suku}>{item.name}</Text>
                  </View>
                </View>
              </TouchableScale>
            ))}

            <Modal
              animationType="fade"
              visible={this.state.isModalVisible}
              onShow={() => this.componentDidMount}
              transparent={true}
              onRequestClose={() => this.componentDidMount()}>
              <View style={styles.modal1}>
                <View style={styles.modal}>
                  <ScrollView>
                    <View style={styles.modalinfounik}>
                      {this.state.isModalVisible === true ? (
                        <Text style={styles.infoheader}>
                          {record[Global.id_infounik - 1].desc}
                        </Text>
                      ) : (
                        <Text />
                      )}
                      <TouchableOpacity
                        style={styles.modalinfodesc}
                        onPress={() => {
                          this.sound(), this.componentDidMount();
                        }}>
                        <Image source={image} style={styles.imginfo} />
                      </TouchableOpacity>
                    </View>
                  </ScrollView>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default InfoUnik;
