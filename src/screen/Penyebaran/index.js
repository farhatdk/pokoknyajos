import * as React from 'react';
import Sound from 'react-native-sound';
import Global from '../../global';
import {
  View,
  Text,
  ScrollView,
  Image,
  ImageBackground,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StatusBar} from 'react-native';
import TouchableScale from 'react-native-touchable-scale';
import styles from '../../../components/styling';
import {ShadowBox} from 'react-native-neomorph-shadows';
const buttonPress = new Sound(
  require('../../../assets/sounds/click_sound.mp3'),
);
const images = require('../../../assets/images/backwhite.webp');
const topimg = require('../../../assets/images/topimg.webp');
const backflip = require('../../../assets/images/backflip.webp');

let SQLite = require('react-native-sqlite-storage');
class Penyebaran extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      record: [],
    };
  }

  componentDidMount() {
    this.setState({open: false});
    BackHandler.addEventListener('hardwareBackPress', this.backAction);

    let db = SQLite.openDatabase({
      name: 'tabelkabupaten.db',
      createFromLocation: '~sukuindo.db',
      location: 'Library',
    });
    db.transaction(tx => {
      tx.executeSql(
        'SELECT tabelkabupaten.name, tabelkabupaten.id, tabelkabupaten.provinsi_id, tabelprovinsi.color, tabelprovinsi.name_route FROM tabelkabupaten INNER JOIN tabelprovinsi ON tabelprovinsi.id = tabelkabupaten.provinsi_id WHERE tabelkabupaten.provinsi_id=' +
          Global.id,
        [],
        (tx, results) => {
          var len = results.rows.length;
          let data = [];
          for (var i = 0; i < len; i++) {
            var row = results.rows.item(i);
            data.push(row);
          }
          this.setState({
            record: data,
          });
        },
      );
    });
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
  }
  tabeldetail = async value => {
    Global.id_detail = value;
  };
  backAction = () => {
    this.props.navigation.goBack();
    return true;
  };
  navigate(name) {
    this.props.navigation.navigate(name);
  }
  sound() {
    buttonPress.play();
  }
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Penyebaran',
      header: null,
      screen: Penyebaran,
    };
  };
  render() {
    const {record} = this.state;
    return (
      <ScrollView>
        <StatusBar
          backgroundColor={record.length > 0 ? record[0].color : ''}
          barStyle="light-content"
        />

        <ShadowBox
          style={{
            backgroundColor: record.length > 0 ? record[0].color : ' ',
            marginBottom: hp(5),
            borderBottomRightRadius: 30,
            borderBottomLeftRadius: 30,
            shadowOffset: {width: 0, height: 10},
            shadowOpacity: 0.5,
            shadowColor: '#959494',
            shadowRadius: 18,
            height: hp(40),
            width: wp(100),
          }}>
          <View style={{marginBottom: hp(5)}} animationIn="fade">
            <TouchableOpacity
              onPress={() => {
                this.sound(), this.props.navigation.goBack();
              }}>
              <Image style={styles.image} source={images} />
            </TouchableOpacity>
            <View style={styles.component} />
            <View style={styles.component2} />
            <View style={styles.component3} />
            <View style={styles.header}>
              {record.length > 0 ? (
                <Text style={styles.teks}>
                  {JSON.parse(JSON.stringify(record[0].name_route))}
                </Text>
              ) : (
                <Text />
              )}
            </View>
            <ImageBackground style={styles.images} source={topimg} />
          </View>
        </ShadowBox>
        {record.map(item => (
          <TouchableScale
            activeScale={1.05}
            friction={5}
            onPress={() => {
              this.sound(), this.navigate('Detail'), this.tabeldetail(item.id);
            }}>
            <View style={styles.menu}>
              <View
                style={{
                  width: wp(90),
                  height: hp(12),
                  backgroundColor: record.length > 0 ? record[0].color : ' ',
                  paddingTop: hp(4),
                  paddingLeft: wp(5),
                  marginBottom: hp(4),
                  borderRadius: 20,
                }}>
                <Image source={backflip} style={styles.backflip} />
                <Text style={styles.suku}>{item.name}</Text>
              </View>
            </View>
          </TouchableScale>
        ))}
      </ScrollView>
    );
  }
}
export default Penyebaran;
