import IntroScreen from './IntroScreen';
import Home from './Home';
import Penyebaran from './Penyebaran';
import Detail from './Detail';
import InfoUnik from './InfoUnik';
import Quiz from './Quiz';
export {IntroScreen, Home, Penyebaran, Detail, InfoUnik, Quiz};
