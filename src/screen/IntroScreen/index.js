import React, {Component} from 'react';
import Sound from 'react-native-sound';
import {AppRegistry, View, Text, ImageBackground} from 'react-native';
import TouchableScale from 'react-native-touchable-scale';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StatusBar} from 'react-native';
import Swiper from 'react-native-swiper';
import styles from '../../../components/styling.js';
const buttonPress = new Sound(
  require('../../../assets/sounds/click_sound.mp3'),
);
const image = require('../../../assets/images/intro.webp');
const image1 = require('../../../assets/images/intro1.webp');
const image2 = require('../../../assets/images/intro2.webp');

class IntroScreen extends Component {
  navigate(name) {
    this.props.navigation.navigate(name);
  }
  sound() {
    buttonPress.play();
  }
  static navigationOptions = ({navigation}) => {
    return {
      title: 'IntroScreen',
      header: null,
      screen: IntroScreen,
    };
  };

  render() {
    return (
      <Swiper
        style={styles.wrapper}
        showsPagination={true}
        showsButtons={false}
        loop={false}
        autoplay={true}
        autoplayTimeout={2}
        activeDotColor={'white'}
        activeDotStyle={{width: wp(5)}}
        dotColor={'white'}
        in>
        <View style={styles.slide}>
          <StatusBar backgroundColor="#FEB473" barStyle="light-content" />
          <ImageBackground style={styles.backgroundImage1} source={image} />
        </View>
        <View style={styles.slide}>
          <ImageBackground style={styles.backgroundImage} source={image1} />
        </View>
        <View style={styles.slide}>
          <ImageBackground style={styles.backgroundImage2} source={image2} />
          <TouchableScale
            activeScale={1.05}
            friction={5}
            onPress={() => {
              this.sound(), this.navigate('Home');
            }}>
            <View style={styles.buttonstart}>
              <Text style={styles.textbutton}>Mulai</Text>
            </View>
          </TouchableScale>
        </View>
      </Swiper>
    );
  }
}

AppRegistry.registerComponent('sukuindo', () => SwiperComponent);

export default IntroScreen;
