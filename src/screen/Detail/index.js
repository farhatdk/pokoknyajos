import React, {Component} from 'react';
import {
  Dimensions,
  Image,
  Text,
  View,
  ImageBackground,
  BackHandler,
} from 'react-native';
import Global from '../../global';
import TouchableScale from 'react-native-touchable-scale';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StatusBar} from 'react-native';
import {TouchableOpacity, ScrollView} from 'react-native-gesture-handler';
import styles from '../../../components/styling.js';
import Sound from 'react-native-sound';
const buttonPress = new Sound(
  require('../../../assets/sounds/click_sound.mp3'),
);
const Aceh = new Sound(require('../../../assets/sounds/Sumatera/aceh.mp3'));
const Batak = new Sound(require('../../../assets/sounds/Sumatera/batak.mp3'));
const Minangkabau = new Sound(
  require('../../../assets/sounds/Sumatera/minang.mp3'),
);
const Nias = new Sound(require('../../../assets/sounds/Sumatera/nias.mp3'));
const Jambi = new Sound(require('../../../assets/sounds/Sumatera/jambi.mp3'));
const Lampung = new Sound(
  require('../../../assets/sounds/Sumatera/lampung.mp3'),
);
const Palembang = new Sound(
  require('../../../assets/sounds/Sumatera/palembang.mp3'),
);
const Melayu = new Sound(require('../../../assets/sounds/Sumatera/melayu.mp3'));
const LainnyaSumatera = new Sound(
  require('../../../assets/sounds/Sumatera/lainnya.mp3'),
);
const Jawa = new Sound(require('../../../assets/sounds/Jawa/jawa.mp3'));
const Betawi = new Sound(require('../../../assets/sounds/Jawa/betawi.mp3'));
const Banten = new Sound(require('../../../assets/sounds/Jawa/banten.mp3'));
const Madura = new Sound(require('../../../assets/sounds/Jawa/madura.mp3'));
const Cirebon = new Sound(require('../../../assets/sounds/Jawa/cirebon.mp3'));
const Sunda = new Sound(require('../../../assets/sounds/Jawa/sunda.mp3'));
const Banjar = new Sound(
  require('../../../assets/sounds/Kalimantan/banjar.mp3'),
);
const Dayak = new Sound(require('../../../assets/sounds/Kalimantan/dayak.mp3'));
const Kutai = new Sound(require('../../../assets/sounds/Kalimantan/kutai.mp3'));
const LainnyaKalimantan = new Sound(
  require('../../../assets/sounds/Kalimantan/lainnya.mp3'),
);
const Bugis = new Sound(require('../../../assets/sounds/Sulawesi/bugis.mp3'));
const Gorontalo = new Sound(
  require('../../../assets/sounds/Sulawesi/gorontalo.mp3'),
);
const LainnyaSulawesi = new Sound(
  require('../../../assets/sounds/Sulawesi/lainnya.mp3'),
);
const Makassar = new Sound(
  require('../../../assets/sounds/Sulawesi/makassar.mp3'),
);
const Minahasa = new Sound(
  require('../../../assets/sounds/Sulawesi/minahasa.mp3'),
);
const Toraja = new Sound(require('../../../assets/sounds/Sulawesi/toraja.mp3'));
const Bali = new Sound(require('../../../assets/sounds/Bali/Bali.mp3'));
const Dompu = new Sound(require('../../../assets/sounds/Bali/Dompu.mp3'));
const Bima = new Sound(require('../../../assets/sounds/Bali/Bima.mp3'));
const Donggo = new Sound(require('../../../assets/sounds/Bali/Donggo.mp3'));
const Flores = new Sound(require('../../../assets/sounds/Bali/Flores.mp3'));
const LainnyaNusaTenggara = new Sound(
  require('../../../assets/sounds/Bali/lainnya.mp3'),
);
const Sasak = new Sound(require('../../../assets/sounds/Bali/sasak.mp3'));
const Sumbawa = new Sound(require('../../../assets/sounds/Bali/sumbawa.mp3'));
const Asmat = new Sound(require('../../../assets/sounds/Papua/asmat.mp3'));
const Dani = new Sound(require('../../../assets/sounds/Papua/dani.mp3'));
const Maluku = new Sound(require('../../../assets/sounds/Papua/maluku.mp3'));
const Papua = new Sound(require('../../../assets/sounds/Papua/papua.mp3'));
const Ternate = new Sound(require('../../../assets/sounds/Papua/ternate.mp3'));

const info2 = require('../../../assets/images/backwhite.webp');
const AcehGambar = require('../../../assets/images/Aceh.jpg');
const BatakGambar = require('../../../assets/images/Batak.jpg');
const MinangkabauGambar = require('../../../assets/images/Minangkabau.webp');
const NiasGambar = require('../../../assets/images/Nias.webp');
const JambiGambar = require('../../../assets/images/Jambi.jpg');
const LampungGambar = require('../../../assets/images/Lampung.jpg');
const PalembangGambar = require('../../../assets/images/Palembang.jpg');
const MelayuGambar = require('../../../assets/images/Melayu.jpg');
const LainnyaSumateraGambar = require('../../../assets/images/LainnyaSumatera.jpg');
const JawaGambar = require('../../../assets/images/Jawa1.jpg');
const BetawiGambar = require('../../../assets/images/Betawi.jpg');
const BantenGambar = require('../../../assets/images/Banten.jpg');
const MaduraGambar = require('../../../assets/images/Madura.jpg');
const CirebonGambar = require('../../../assets/images/Cirebon.jpg');
const SundaGambar = require('../../../assets/images/Sunda.webp');
const BanjarGambar = require('../../../assets/images/Banjar.jpg');
const DayakGambar = require('../../../assets/images/Dayak.jpg');
const KutaiGambar = require('../../../assets/images/Kutai.jpg');
const LainnyaKalimantanGambar = require('../../../assets/images/LainnyaKalimantan.jpg');
const BugisGambar = require('../../../assets/images/Bugis.jpg');
const GorontaloGambar = require('../../../assets/images/Gorontalo.jpg');
const LainnyaSulawesiGambar = require('../../../assets/images/LainnyaSulawesi.jpg');
const MakassarGambar = require('../../../assets/images/Makassar.jpg');
const MinahasaGambar = require('../../../assets/images/Minahasa.webp');
const TorajaGambar = require('../../../assets/images/Toraja.webp');
const BaliGambar = require('../../../assets/images/Bali.jpg');
const DompuGambar = require('../../../assets/images/Dompu.jpg');
const BimaGambar = require('../../../assets/images/Bima.jpg');
const DonggoGambar = require('../../../assets/images/Donggo.jpg');
const FloresGambar = require('../../../assets/images/Flores.jpg');
const LainnyaNusaTenggaraGambar = require('../../../assets/images/LainnyaNusaTenggara.jpg');
const SasakGambar = require('../../../assets/images/Sasak.webp');
const SumbawaGambar = require('../../../assets/images/Sumbawa.webp');
const AsmatGambar = require('../../../assets/images/Asmat.jpg');
const DaniGambar = require('../../../assets/images/Dani.jpeg');
const MalukuGambar = require('../../../assets/images/Maluku.jpg');
const PapuaGambar = require('../../../assets/images/Papua1.webp');
const TernateGambar = require('../../../assets/images/Ternate.webp');

const play = require('../../../assets/images/play.webp');

const sound = [
  {
    sounds: Aceh,
  },
  {
    sounds: Batak,
  },
  {
    sounds: Minangkabau,
  },
  {
    sounds: Nias,
  },
  {
    sounds: Jambi,
  },
  {
    sounds: Lampung,
  },
  {
    sounds: Palembang,
  },
  {
    sounds: Melayu,
  },
  {
    sounds: LainnyaSumatera,
  },
  {
    sounds: Jawa,
  },
  {
    sounds: Betawi,
  },
  {
    sounds: Banten,
  },
  {
    sounds: Madura,
  },
  {
    sounds: Cirebon,
  },
  {
    sounds: Sunda,
  },
  {
    sounds: Banjar,
  },
  {
    sounds: Dayak,
  },
  {
    sounds: Kutai,
  },
  {
    sounds: LainnyaKalimantan,
  },
  {
    sounds: Bugis,
  },
  {
    sounds: Gorontalo,
  },
  {
    sounds: Makassar,
  },
  {
    sounds: LainnyaSulawesi,
  },
  {
    sounds: Minahasa,
  },
  {
    sounds: Toraja,
  },
  {
    sounds: Bali,
  },
  {
    sounds: Dompu,
  },
  {
    sounds: Bima,
  },
  {
    sounds: Donggo,
  },
  {
    sounds: Flores,
  },
  {
    sounds: LainnyaNusaTenggara,
  },
  {
    sounds: Sasak,
  },
  {
    sounds: Sumbawa,
  },
  {
    sounds: Asmat,
  },
  {
    sounds: Dani,
  },
  {
    sounds: Maluku,
  },
  {
    sounds: Papua,
  },
  {
    sounds: Ternate,
  },
];
const image = [
  {
    images: AcehGambar,
  },
  {
    images: BatakGambar,
  },
  {
    images: MinangkabauGambar,
  },
  {
    images: NiasGambar,
  },
  {
    images: JambiGambar,
  },
  {
    images: LampungGambar,
  },
  {
    images: PalembangGambar,
  },
  {
    images: MelayuGambar,
  },
  {
    images: LainnyaSumateraGambar,
  },
  {
    images: JawaGambar,
  },
  {
    images: BetawiGambar,
  },
  {
    images: BantenGambar,
  },
  {
    images: MaduraGambar,
  },
  {
    images: CirebonGambar,
  },
  {
    images: SundaGambar,
  },
  {
    images: BanjarGambar,
  },
  {
    images: DayakGambar,
  },
  {
    images: KutaiGambar,
  },
  {
    images: LainnyaKalimantanGambar,
  },
  {
    images: BugisGambar,
  },
  {
    images: GorontaloGambar,
  },
  {
    images: LainnyaSulawesiGambar,
  },
  {
    images: MakassarGambar,
  },
  {
    images: MinahasaGambar,
  },
  {
    images: TorajaGambar,
  },
  {
    images: BaliGambar,
  },
  {
    images: DompuGambar,
  },
  {
    images: BimaGambar,
  },
  {
    images: DonggoGambar,
  },
  {
    images: FloresGambar,
  },
  {
    images: LainnyaNusaTenggaraGambar,
  },
  {
    images: SasakGambar,
  },
  {
    images: SumbawaGambar,
  },
  {
    images: AsmatGambar,
  },
  {
    images: DaniGambar,
  },
  {
    images: MalukuGambar,
  },
  {
    images: PapuaGambar,
  },
  {
    images: TernateGambar,
  },
];

let SQLite = require('react-native-sqlite-storage');
class Detail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      record: '',
    };
  }

  componentDidMount() {
    this.setState({open: false});
    BackHandler.addEventListener('hardwareBackPress', this.backAction);
    let db = SQLite.openDatabase({
      name: 'tabeldetail.db',
      createFromLocation: '~sukuindo.db',
      location: 'Library',
    });
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM tabeldetail WHERE kabupaten_id= ' + Global.id_detail,
        [],
        (tx, results) => {
          var len = results.rows.length;
          for (var i = 0; i < len; i++) {
            var row = results.rows.item(i);
            this.setState({
              record: row,
            });
          }
        },
      );
    });
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
    sound[Global.id_detail - 1].sounds.stop();
  }
  backAction = () => {
    this.props.navigation.goBack();
    return true;
  };
  sound() {
    sound[Global.id_detail - 1].sounds.play();
  }
  soundback() {
    buttonPress.play();
  }
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Detail',
      header: null,
      screen: Detail,
    };
  };
  render() {
    return (
      <View>
        <ScrollView>
          <StatusBar hidden={true} barStyle="light-content" />

          <ImageBackground
            style={{
              width: window.width,
              height: PARALLAX_HEADER_HEIGHT,
            }}
            source={image[Global.id_detail - 1].images}>
            <TouchableOpacity
              onPress={() => {
                this.soundback(), this.props.navigation.goBack();
              }}>
              <Image style={styles.backButton} source={info2} />
            </TouchableOpacity>
            <View style={styles.parallaxHeader}>
              <Text style={styles.sectionSpeakerText}>
                {this.state.record.name}
              </Text>
            </View>
          </ImageBackground>

          <View style={styles.container}>
            <Text style={styles.section}>Jenis Suku</Text>
            <View style={styles.boxtext}>
              <Text style={styles.textinside}>
                {this.state.record.jenis_suku}
              </Text>
            </View>
            <Text style={styles.section}>Jumlah Suku</Text>
            <View style={styles.boxtext}>
              <Text style={styles.textinside}>
                {this.state.record.jml_suku} Jiwa
              </Text>
            </View>
            {this.state.record.lokasi_suku ? (
              <View>
                <Text style={styles.section}>Lokasi Suku</Text>
                <View style={styles.boxtext}>
                  <Text style={styles.textinside}>
                    {this.state.record.lokasi_suku}
                  </Text>
                </View>
                <Text style={styles.section}>Bahasa Daerah</Text>
                <View style={styles.boxtext}>
                  <Text style={styles.textinside}>
                    {this.state.record.bahasa}
                  </Text>
                </View>
                <Text style={styles.section}>Mata Pencarian</Text>
                <View style={styles.boxtext}>
                  <Text style={styles.textinside}>
                    {this.state.record.mata_pencaharian}
                  </Text>
                </View>
                <Text style={styles.section}>Rumah Adat</Text>
                <View style={styles.boxtext}>
                  <Text style={styles.textinside}>
                    {this.state.record.rmh_adat}
                  </Text>
                </View>
                <Text style={styles.section}>Senjata Tradisional</Text>
                <View style={styles.boxtext}>
                  <Text style={styles.textinside}>
                    {this.state.record.snjt_tradisional}
                  </Text>
                </View>
              </View>
            ) : (
              <View />
            )}
          </View>
        </ScrollView>

        <View style={{position: 'absolute', bottom: hp(3), right: wp(5)}}>
          <TouchableScale
            activeScale={1.05}
            friction={5}
            onPress={() => this.sound()}>
            <View style={styles.buttonPlay}>
              <Image source={play} style={styles.imagePlay} />
            </View>
          </TouchableScale>
        </View>
      </View>
    );
  }
}
const window = Dimensions.get('window');
const PARALLAX_HEADER_HEIGHT = 320;

export default Detail;
