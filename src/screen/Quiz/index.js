import React, {Component} from 'react';
import Sound from 'react-native-sound';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  BackHandler,
  TouchableHighlight,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import TouchableScale from 'react-native-touchable-scale';
import styles from '../../../components/styling2';
import {StatusBar} from 'react-native';
import QuestionsSrc from '../../../components/QuestionSrc';
const buttonPress = new Sound(
  require('../../../assets/sounds/click_sound.mp3'),
);
const image = require('../../../assets/images/backwhite.webp');
const quiz = require('../../../assets/images/quiz.webp');
const bck = require('../../../assets/images/backwhite.webp');

class Quiz extends React.Component {
  constructor(props) {
    super(props);
    this.startQuiz = this.startQuiz.bind(this);
    this.answerQuestion = this.answerQuestion.bind(this);
    this.reset = this.reset.bind(this);
    this.state = {
      isStarted: false,
      endQuiz: false,
      questionIdx: 0,
      questionsArr: [0, 1, 2],
      score: 0,
      currentQuestion: 0,
      currentChoices: 0,
      currentAnswer: 0,
    };
  }
  _navigate(route) {
    this.props.navigator.push({
      name: route,
    });
  }
  sound() {
    buttonPress.play();
  }
  navigate(name) {
    this.props.navigation.navigate(name);
  }
  componentDidMount() {
    this.setUpQuestions();
    BackHandler.addEventListener('hardwareBackPress', this.backAction);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
  }
  backAction = () => {
    this.props.navigation.navigate('Home');
    return true;
  };
  setUpQuestions() {
    let questionsArr = [];
    for (let i = 0; i < 10; i++) {
      let rand = Math.round(Math.random() * 30);
      while (questionsArr.indexOf(rand) > -1) {
        rand = Math.round(Math.random() * 30);
      }
      questionsArr.push(rand);
    }
    this.setState({
      questionsArr: questionsArr,
    });
  }
  updateQuestion(newIdx) {
    let src = QuestionsSrc[this.state.questionsArr[newIdx]];
    this.setState({
      currentQuestion: src.question,
      currentChoices: [
        src.choices[0],
        src.choices[1],
        src.choices[2],
        src.choices[3],
      ],
      currentAnswer: src.answer,
    });
  }
  startQuiz() {
    buttonPress.play();
    this.updateQuestion(0);
    this.setState({
      isStarted: true,
    });
  }
  answerQuestion(choice) {
    buttonPress.play();
    if (choice == this.state.currentAnswer) {
      this.setState({
        score: this.state.score + 10,
      });
    }
    if (this.state.questionIdx >= 9) {
      this.endQuiz();
    } else {
      this.nextQuestion();
    }
  }
  nextQuestion() {
    let newIdx = this.state.questionIdx + 1;
    this.setState(
      {
        questionIdx: newIdx,
      },
      this.updateQuestion(newIdx),
    );
  }
  endQuiz() {
    this.setState({
      endQuiz: true,
    });
  }
  reset() {
    buttonPress.play();
    this.setState({
      isStarted: false,
      endQuiz: false,
      questionIdx: 0,
      questionsArr: [0, 1, 2],
      score: 0,
      currentQuestion: 0,
      currentChoices: 0,
      currentAnswer: 0,
    });
    this.setUpQuestions();
  }
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Quiz',
      header: null,
      screen: Quiz,
    };
  };
  render() {
    let content = null;
    let top = null;

    if (this.state.isStarted == false) {
      content = (
        <StartButton startQuiz={this.startQuiz} onPress={() => this.sound()} />
      );
      top = (
        <View>
          <ImageBackground source={quiz} style={styles.backgroundImage} />
          <Text style={styles.title}>Yuk Tes{'\n'}Pengetahuan Kamu !</Text>
        </View>
      );
    } else if (this.state.isStarted == true && this.state.endQuiz == false) {
      content = (
        <Question
          question={this.state.currentQuestion}
          choices={this.state.currentChoices}
          answerQuestion={this.answerQuestion}
        />
      );
      top = <Text style={styles.question}>{this.state.currentQuestion}</Text>;
    } else {
      content = (
        <EndQuiz
          score={this.state.score}
          reset={this.reset}
          navigation={() => this.props.navigation.navigate('Home')}
        />
      );
      top = <Text style={styles.title}>{this.state.score}0%</Text>;
    }
    return (
      <View>
        <StatusBar backgroundColor="#5375FF" barStyle="light-content" />

        <View
          style={
            this.state.isStarted == false ? styles.wrapper : styles.wrapperPutih
          }>
          <TouchableOpacity
            onPress={() => {
              this.sound(), this.props.navigation.goBack();
            }}>
            <Image source={bck} style={styles.headquiz} />
          </TouchableOpacity>
          <View style={styles.top}>{top}</View>
          {content}
        </View>
      </View>
    );
  }
}
class StartButton extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'space-around',
        }}>
        <TouchableScale
          activeScale={1.05}
          friction={5}
          style={styles.startquiz}
          onPress={() => this.props.startQuiz()}
          underlayColor="rgba(215, 147, 63,.3)">
          <Text style={styles.mulai}>Mulai</Text>
        </TouchableScale>
      </View>
    );
  }
}
class Question extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.content}>
        <View
          style={{
            width: wp(75),
            height: hp(40),
            borderRadius: 150,
            backgroundColor: '#5375FF',
            opacity: 0.1,
            position: 'absolute',
            marginTop: hp(-35),
            marginLeft: wp(-25),
          }}
        />
        <View
          style={{
            width: wp(37),
            height: hp(20),
            borderRadius: 150,
            backgroundColor: '#5375FF',
            opacity: 0.1,
            position: 'absolute',
            marginTop: hp(30),
            marginLeft: wp(65),
          }}
        />
        <View
          style={{
            width: wp(20),
            height: hp(11),
            borderRadius: 150,
            backgroundColor: '#5375FF',
            opacity: 0.1,
            position: 'absolute',
            marginTop: hp(40),
            marginLeft: wp(55),
          }}
        />
        <View style={styles.choiceRow}>
          <TouchableScale
            activeScale={1.05}
            friction={5}
            onPress={this.props.answerQuestion.bind(
              this,
              this.props.choices[0],
            )}
            style={styles.choice}
            underlayColor="rgba(215, 147, 63,.3)">
            <Text style={styles.choiceText}>{this.props.choices[0]}</Text>
          </TouchableScale>
          <TouchableScale
            activeScale={1.05}
            friction={5}
            onPress={this.props.answerQuestion.bind(
              this,
              this.props.choices[1],
            )}
            style={styles.choice}
            underlayColor="rgba(215, 147, 63,.3)">
            <Text style={styles.choiceText}>{this.props.choices[1]}</Text>
          </TouchableScale>
          <TouchableScale
            activeScale={1.05}
            friction={5}
            onPress={this.props.answerQuestion.bind(
              this,
              this.props.choices[2],
            )}
            style={styles.choice}
            underlayColor="rgba(215, 147, 63,.3)">
            <Text style={styles.choiceText}>{this.props.choices[2]}</Text>
          </TouchableScale>
          <TouchableScale
            activeScale={1.05}
            friction={5}
            onPress={this.props.answerQuestion.bind(
              this,
              this.props.choices[3],
            )}
            style={styles.choice}
            underlayColor="rgba(215, 147, 63,.3)">
            <Text style={styles.choiceText}>{this.props.choices[3]}</Text>
          </TouchableScale>
        </View>
      </View>
    );
  }
}
class EndQuiz extends React.Component {
  render() {
    let message = null;
    if (this.props.score == 100) {
      message = `Skor kamu ${this.props.score} Sempurna! Kamu jago banget`;
    } else if (this.props.score > 80) {
      message = `Skor kamu ${this.props.score} Hebat! Kamu keren`;
    } else if (this.props.score > 50) {
      message = `Skor kamu ${this.props.score} Belajar lagi ya!`;
    } else if (this.props.score > 10) {
      message = `Skor kamu ${this.props.score} Semangat, Coba lagi yuk!`;
    } else {
      message = `Skor kamu ${this.props.score} Dibaca lagi yuk!`;
    }
    const iniPress = () => {
      this.props.navigation.navigate('Home');
    };
    return (
      <View style={styles.contentSpaceBetween}>
        <Text style={styles.numDigits}>{message}</Text>
        <View>
          <TouchableScale
            activeScale={1.05}
            friction={5}
            style={styles.roundedBtn}
            onPress={() => this.props.reset()}
            underlayColor="rgba(215, 147, 63,0.3)">
            <Text style={styles.roundedBtnText}>Coba Lagi ?</Text>
          </TouchableScale>
        </View>
      </View>
    );
  }
}
export default Quiz;
