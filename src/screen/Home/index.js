import * as React from 'react';
import Sound from 'react-native-sound';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  BackHandler,
  Alert,
} from 'react-native';
import TouchableScale from 'react-native-touchable-scale';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StatusBar} from 'react-native';
import SwiperCom from '../../../components/HomePage/SwiperCom';
import Global from '../../global';
import {ShadowBox} from 'react-native-neomorph-shadows';
const Sumatera = require('../../../assets/images/sumatera-icon.webp');
const Jawa = require('../../../assets/images/jawa-icon.webp');
const Kalimantan = require('../../../assets/images/kalimantan-icon.webp');
const Sulawesi = require('../../../assets/images/sulawesi-icon.webp');
const Bali = require('../../../assets/images/bali-icon.webp');
const Papua = require('../../../assets/images/papua-icon.webp');
const iicon = require('../../../assets/images/i-icon.webp');
const questionicon = require('../../../assets/images/question-icon.webp');
const buttonPress = new Sound(
  require('../../../assets/sounds/click_sound.mp3'),
);

const image = [
  {
    images: Sumatera,
  },
  {
    images: Jawa,
  },
  {
    images: Kalimantan,
  },
  {
    images: Sulawesi,
  },
  {
    images: Bali,
  },
  {
    images: Papua,
  },
];
let SQLite = require('react-native-sqlite-storage');
class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      record: [],
    };
  }
  componentDidMount() {
    this.setState({open: false});
    BackHandler.addEventListener('hardwareBackPress', this.backAction);
    var db = SQLite.openDatabase({
      name: 'tabelprovinsi.db',
      createFromLocation: '~sukuindo.db',
      location: 'Library',
    });
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM tabelprovinsi', [], (tx, results) => {
        var len = results.rows.length;
        let data = [];
        for (var i = 0; i < len; i++) {
          var row = results.rows.item(i);
          data.push(row);
        }
        this.setState({
          record: data,
        });
      });
    });
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
  }
  navigate(name) {
    this.props.navigation.navigate(name);
  }
  sound() {
    buttonPress.play();
  }
  storeData = async value => {
    Global.id = value;
  };
  static navigationOptions = ({}) => {
    return {
      title: 'Home',
      header: null,
      screen: Home,
    };
  };

  backAction = () => {
    Alert.alert('Sebentar', 'Apa kamu mau keluar aplikasi ini?', [
      {
        text: 'Tidak',
        onPress: () => null,
        style: 'Tidak',
      },
      {text: 'Iya', onPress: () => BackHandler.exitApp()},
    ]);
    return true;
  };

  render() {
    const {record} = this.state;
    return (
      <ScrollView style={styles.view} showsVerticalScrollIndicator={false}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <View>
          <Text style={styles.text}>Ketahui Penyebaran{'\n'}Suku</Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {record.map((item, index) => (
              <TouchableScale
                activeScale={1.05}
                friction={5}
                onPress={() => {
                  this.sound(),
                    this.navigate('Penyebaran'),
                    this.storeData(item.id);
                }}>
                <View
                  style={{
                    width: wp(61),
                    height: hp(38),
                    marginLeft: wp(5),
                    marginRight: wp(5),
                    borderRadius: 20,
                    marginBottom: hp(6),
                    fontFamily: 'GilroyBold',
                    marginTop: hp(2),
                    paddingLeft: wp(4),
                    backgroundColor: item.color,
                    justifyContent: 'center',
                  }}>
                  <Image style={styles.icon} source={image[index].images} />

                  <Text style={styles.textinside}>{item.name}</Text>
                </View>
              </TouchableScale>
            ))}
          </ScrollView>

          <ShadowBox
            useSvg={true}
            style={{
              shadowOffset: {width: 0, height: 0},
              shadowOpacity: 0.5,
              shadowColor: '#959494',
              shadowRadius: 18,
              borderTopRightRadius: 30,
              borderTopLeftRadius: 30,
              backgroundColor: '#FEB973',
              width: wp(100),
              paddingBottom: hp(5),
              paddingTop: hp(4),
              height: hp(92),
            }}>
            <View style={{flex: 1, paddingBottom: hp(5.5)}}>
              <View
                style={{
                  width: wp(5.5),
                  height: hp(3),
                  borderRadius: 12,
                  backgroundColor: 'white',
                  marginLeft: wp(5),
                  opacity: 0.3,
                }}
              />
              <View
                style={{
                  width: wp(5.5),
                  height: hp(3),
                  borderRadius: 12,
                  backgroundColor: 'white',
                  marginLeft: wp(13),
                  opacity: 0.3,
                  marginTop: hp(-3),
                }}
              />
              <View
                style={{
                  width: wp(5.5),
                  height: hp(3),
                  borderRadius: 12,
                  backgroundColor: 'white',
                  marginLeft: wp(21),
                  opacity: 0.3,
                  marginTop: hp(-3),
                }}
              />
            </View>

            <TouchableScale
              activeScale={1.05}
              friction={5}
              onPress={() => {
                this.sound(), this.navigate('InfoUnik');
              }}>
              <View style={styles.suku5}>
                <Image style={styles.icon3} source={iicon} />
                <Text style={styles.textbutton}>Info Unik</Text>
              </View>
            </TouchableScale>
            <TouchableScale
              activeScale={1.05}
              friction={5}
              onPress={() => {
                this.sound(), this.navigate('Quiz');
              }}>
              <View style={styles.suku6}>
                <Image style={styles.icon3} source={questionicon} />
                <Text style={styles.textbutton}>Kuis Suku</Text>
              </View>
            </TouchableScale>
            <View>
              <Text style={styles.font3}>Galeri Suku{'\n'}Indonesia</Text>
            </View>
            <SwiperCom />
          </ShadowBox>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
  },
  imgnext: {
    width: wp(17),
    height: hp(9.5),
    marginLeft: wp(5),
    marginTop: hp(22),
  },
  textbutton: {
    marginLeft: wp(25),
    fontSize: hp(3.5),
    fontFamily: 'GilroyBold',
    color: '#818181',
    marginTop: hp(-9.5),
  },
  icon: {
    marginTop: hp(-3),
    marginBottom: hp(2),
    width: wp(32),
    height: hp(22),
  },
  icon2: {
    width: wp(32),
    height: hp(22),
    marginTop: hp(0),
  },

  icon3: {
    marginTop: hp(0.5),
    width: wp(13),
    height: hp(13),
    marginLeft: wp(5),
  },
  suku6: {
    width: wp(90),
    marginTop: hp(4),
    height: hp(13),
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    marginLeft: wp(5),
    marginRight: wp(5),
  },
  suku5: {
    width: wp(90),
    height: hp(13),
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    marginLeft: wp(5),
    marginRight: wp(5),
  },
  font3: {
    fontSize: hp(4),
    fontWeight: 'bold',
    color: 'white',
    marginTop: hp(5),
    zIndex: 2,
    marginLeft: wp(5),
  },
  textinside: {
    fontSize: hp(3.2),
    color: 'white',
    fontFamily: 'GilroyBold',
    marginTop: hp(4),
    paddingLeft: wp(5),
    paddingRight: wp(5),
    marginRight: wp(3),
    textAlign: 'right',
  },
  textinside2: {
    fontSize: hp(3.3),
    color: 'white',
    fontFamily: 'GilroyBold',
    marginBottom: hp(10),
    paddingLeft: wp(5),
    paddingTop: hp(2.2),
    paddingRight: wp(5),
    textAlign: 'right',
  },
  button: {
    width: wp(61),
    height: hp(35),
    marginLeft: wp(5),
    borderRadius: 20,
    marginBottom: hp(6),
    fontFamily: 'GilroyBold',
    marginTop: hp(2),
    paddingLeft: wp(4),
    backgroundColor: '#5375FF',
  },
  button2: {
    width: wp(63),
    height: hp(35),
    marginLeft: wp(5),
    borderRadius: 20,
    marginBottom: hp(6),
    fontFamily: 'GilroyBold',
    marginTop: hp(3),
    paddingLeft: wp(4),
    backgroundColor: '#FF9C9C',
  },
  button3: {
    width: wp(63),
    height: hp(35),
    marginLeft: wp(5),
    borderRadius: 20,
    marginBottom: hp(6),
    fontFamily: 'GilroyBold',
    marginTop: hp(3),
    paddingLeft: wp(4),
    backgroundColor: '#8CBA51',
  },
  button4: {
    width: wp(63),
    height: hp(35),
    marginLeft: wp(5),
    borderRadius: 20,
    marginBottom: hp(6),
    paddingLeft: wp(4),
    fontFamily: 'GilroyBold',
    marginTop: hp(3),
    backgroundColor: '#8FD0F6',
  },
  button5: {
    width: wp(63),
    height: hp(35),
    marginLeft: wp(5),
    borderRadius: 20,
    marginBottom: hp(6),
    paddingLeft: wp(4),
    fontFamily: 'GilroyBold',
    marginTop: hp(3),
    backgroundColor: '#BF86EF',
  },
  button6: {
    width: wp(63),
    height: hp(35),
    marginLeft: wp(5),
    marginRight: wp(5),
    borderRadius: 20,
    marginBottom: hp(6),
    fontFamily: 'GilroyBold',
    marginTop: hp(3),
    paddingLeft: wp(4),
    backgroundColor: '#EB80AE',
  },
  galeri: {
    width: wp(39.5),
    height: hp(27),
    marginTop: hp(95),
    zIndex: 2,
    position: 'absolute',
    marginLeft: wp(57),
  },
  text: {
    marginLeft: wp(5),
    marginTop: hp(4),
    marginBottom: hp(1),
    fontSize: hp(4),
    color: '#4A4A4A',
    fontFamily: 'GilroyBold',
  },
});
export default Home;
